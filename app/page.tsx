'use client'
import { useRouter } from 'next/navigation'
import { ChangeEvent, useEffect, useRef, useState } from 'react'
import { checkAddressExists, checkTransactionExists } from './api'
import { TransactionListCard } from './components'
import { IWebSockTxList } from './dto'
import { calculateTotalFromOut, getExchangeRate } from './utils'

const Home = () => {
  const router = useRouter()
  const [hideAlert, setHideAlert] = useState<boolean>(true)
  const [txList, setTxList] = useState<IWebSockTxList[]>([])
  const [rate, setRate] = useState<number>(0)
  const [item, setItem] = useState<string>('')
  const ws = useRef<WebSocket | null>(null)

  useEffect(() => {
    document.title = 'Homepage'

    ws.current = new WebSocket('wss://ws.blockchain.info/inv')

    ws.current.onopen = () => {
      if (!ws.current) return
      ws.current.send(
        JSON.stringify({
          op: 'unconfirmed_sub',
        }),
      )
    }

    const fetchData = async () => {
      const exchangeRate: number = await getExchangeRate('USD')
      setRate(() => exchangeRate)
    }

    fetchData()

    return () => {
      if (!ws.current) return
      ws.current.send(
        JSON.stringify({
          op: 'unconfirmed_unsub',
        }),
      )

      ws.current.close()
    }
  }, [])

  useEffect(() => {
    if (!ws.current) return

    ws.current.onmessage = (message) => {
      const tx: IWebSockTxList = JSON.parse(message.data) as IWebSockTxList

      if (txList.length >= 6) {
        setTxList((prevList) => prevList.slice(1))
      }

      setTxList((prevList) => [...prevList, tx])
    }
  }, [txList])

  const showResult = async () => {
    try {
      const addressCheck = await checkAddressExists(item)
      const txCheck = await checkTransactionExists(item)
      if (addressCheck) {
        router.push(`/address/${item}`)
      } else if (txCheck) {
        router.push(`/tx/${item}`)
      } else {
        throw new Error('No item have been found')
      }
    } catch (error) {
      setHideAlert(() => false)
    }
  }

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setItem(() => event.target.value)
  }

  return (
    <>
      <main>
        {hideAlert ? (
          <></>
        ) : (
          <>
            <div className="max-w-md mx-auto rounded-xl md:max-w-lg">
              <div
                className="bg-red-100 text-red-600 p-4 mt-5 rounded-xl"
                role="alert">
                <p>Invalid btc address or hash transaction</p>
              </div>
            </div>
          </>
        )}

        <div className="max-w-md mx-auto rounded-xl shadow-md overflow-hidden md:max-w-2xl my-12">
          <div className="flex items-center rounded-full bg-white drop-shadow-md px-4">
            <input
              name="item"
              type="text"
              placeholder="Search Transactions and Addresses"
              className="w-full p-2 outline-none"
              onChange={handleChange}
            />
            <button
              type="button"
              className="text-gray-600 focus:outline-none"
              onClick={showResult}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round">
                <circle cx="11" cy="11" r="8"></circle>
                <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
              </svg>
            </button>
          </div>
        </div>
        {txList.length > 0 ? (
          <>
            {txList.map((item, index) => (
              <TransactionListCard
                key={index}
                hash={item.x.hash}
                time={item.x.time}
                total={calculateTotalFromOut(item.x.out)}
                rate={rate}
              />
            ))}
          </>
        ) : (
          <></>
        )}
      </main>
    </>
  )
}

export default Home
