'use client'
import { useParams } from 'next/navigation'
import { ChangeEvent, useEffect, useState } from 'react'
import { getAddressData, getExchangeRates } from '../../api'
import { BackButton } from '../../components'
import SpinningCircle from '../../components/SpinningCircle'
import {
  IAddress,
  IExchangeRate,
  IExchangeRates,
  addressInitialState,
  currencies,
} from '../../dto'
import { convertByCurrency, reduceHashOrAddress } from '../../utils'

const Address = () => {
  const path = useParams()

  const [address, setAddress] = useState<IAddress>(addressInitialState)

  const [spent, setSpent] = useState<number>(0)
  const [unspent, setUnpent] = useState<number>(0)
  const [currency, setCurrency] = useState<string>('BTC')
  const [isLoading, setIsLoading] = useState<boolean>(true)
  const [rate, setRate] = useState<number>(0)

  useEffect(() => {
    document.title = `Address: ${reduceHashOrAddress(path.id)}`

    const fetchData = async () => {
      try {
        const data = await getAddressData(path.id)
        setAddress(() => ({ ...data }))
        const spentTotal = data.txrefs
          .filter((item) => item.spent === true)
          .reduce((acc, currentValue) => acc + currentValue.value, spent)
        setSpent(() => spentTotal)
        const unspentTotal = data.txrefs
          .filter((item) => item.spent === false)
          .reduce((acc, currentValue) => acc + currentValue.value, unspent)
        setUnpent(() => unspentTotal)
        setIsLoading(() => !isLoading)
      } catch (error) {
        console.error(error)
      }
    }

    fetchData()
  }, [])

  useEffect(() => {
    const fetchData = async () => {
      if (currency !== '' && currency !== 'BTC') {
        const rates: IExchangeRates[] = await getExchangeRates()
        const exchangeRate: IExchangeRate = rates[currency]
        if (exchangeRate === undefined)
          throw new Error('No conversion rate found')
        setRate(() => exchangeRate.buy)
      }
    }

    fetchData()
  }, [currency])

  const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
    setCurrency(() => event.target.value)
  }

  return (
    <>
      <main>
        <div
          className={`max-w-md mx-auto rounded-xl bg-stone-100 shadow-md overflow-hidden ${
            path.id.startsWith('bc') ? 'md:max-w-xl' : 'md:max-w-sm'
          } my-24`}>
          <div className="p-8">
            <BackButton />
            {isLoading ? (
              <div className="flex items-center justify-center">
                <SpinningCircle />
              </div>
            ) : (
              <>
                <div className="inline-flex justify-between w-full">
                  <div className="capitalize tracking-wide text-3xl text-black font-semibold">
                    Bitcoin Address
                  </div>
                  <select
                    className="block w-1/8 py-2 px-3 border border-gray-300 bg-stone-100 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
                    value={currency}
                    onChange={handleChange}>
                    {currencies.map((currency, index) => (
                      <option key={index} value={currency.type}>
                        {currency.type}
                      </option>
                    ))}
                  </select>
                </div>
                <div className="block mt-1 text-lg leading-tight font-medium text-black mt-3">
                  Address ID
                </div>
                <p className="text-gray-500 truncate">{address.address}</p>
                <div className="inline-flex justify-between w-full">
                  <div>
                    <div className="mt-1 text-lg leading-tight font-medium text-black mt-3">
                      Confirmed Transactions
                    </div>
                    <p className="text-gray-500">{address.n_tx}</p>
                  </div>
                  <div>
                    <div className="mt-1 text-lg leading-tight font-medium text-black mt-3">
                      Balance
                    </div>
                    <p className="text-gray-500">
                      {convertByCurrency(address.balance, currency, rate)}
                    </p>
                  </div>
                </div>
                <div className="inline-flex justify-between w-full">
                  <div>
                    <div className="block mt-1 text-lg leading-tight font-medium text-black mt-3">
                      Received
                    </div>
                    <p className="text-gray-500">
                      {convertByCurrency(
                        address.total_received,
                        currency,
                        rate,
                      )}
                    </p>
                  </div>
                  <div>
                    <div className="block mt-1 text-lg leading-tight font-medium text-black mt-3">
                      Spent
                    </div>
                    <p className="text-gray-500">
                      {convertByCurrency(spent, currency, rate)}
                    </p>
                  </div>
                  <div>
                    <div className="block mt-1 text-lg leading-tight font-medium text-black mt-3">
                      Unspent
                    </div>
                    <p className="text-gray-500">
                      {convertByCurrency(unspent, currency, rate)}
                    </p>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </main>
    </>
  )
}

export default Address
