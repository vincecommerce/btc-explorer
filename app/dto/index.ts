// Transaction section start
export interface ITransaction {
  hash: string
  fees: number
  size: number
  received: string
  confirmations: number
  inputs: ITxInputs[]
  outputs: ITxOutputs[]
}

export interface ITxInputs {
  output_value: number
}

export interface ITxOutputs {
  value: number
}

export interface IWebSockTxList {
  x: IWebSockTx
}

export interface IWebSockTx {
  hash: string
  time: number
  out: IWebSockTxOut[]
}

export interface IWebSockTxOut {
  value: number
}

export const txInitialState: ITransaction = {
  hash: '',
  fees: 0,
  size: 0,
  received: '',
  confirmations: 0,
  inputs: [],
  outputs: [],
}
// Transaction section end

// Address section start
export interface IAddress {
  address: string
  total_received: number
  balance: number
  n_tx: number
  txrefs: IAddressTxRefs[]
}

export interface IAddressTxRefs {
  value: number
  spent: boolean
}

export const addressInitialState: IAddress = {
  address: '',
  total_received: 0,
  balance: 0,
  n_tx: 0,
  txrefs: [],
}
// Address section end

// Currency section start
export interface IExchangeRates {
  [key: string]: IExchangeRate
}

export interface IExchangeRate {
  '15m': number
  last: number
  buy: number
  sell: number
  symbol: string
}

export interface ICurrencies {
  type: string
  symbol: string
}

export const currencies: ICurrencies[] = [
  {
    type: 'BTC',
    symbol: 'BTC',
  },
  {
    type: 'EUR',
    symbol: '€',
  },
  {
    type: 'USD',
    symbol: '$',
  },
]

// Currency section end
