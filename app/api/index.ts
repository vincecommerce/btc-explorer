import { IAddress, IExchangeRates, ITransaction } from '../dto'

export const checkTransactionExists = async (
  hash: string,
): Promise<boolean> => {
  const check = await fetch(`/api/cypher/txs/${hash}`, {
    cache: 'no-cache',
  })

  return check.ok
}

export const getTransactionData = async (
  hash: string,
): Promise<ITransaction> => {
  const res = await fetch(`/api/cypher/txs/${hash}`, {
    cache: 'force-cache',
  })

  if (!res.ok) {
    throw new Error('Failed to fetch transaction data')
  }

  const tx: ITransaction = (await res.json()) as ITransaction

  return tx
}

export const checkAddressExists = async (address: string): Promise<boolean> => {
  const check = await fetch(`/api/cypher/addrs/${address}`, {
    cache: 'no-cache',
  })

  return check.ok
}

export const getAddressData = async (address: string): Promise<IAddress> => {
  const res = await fetch(`/api/cypher/addrs/${address}`, {
    cache: 'force-cache',
  })

  if (!res.ok) {
    throw new Error('Failed to fetch address data')
  }

  const tx: IAddress = (await res.json()) as IAddress

  return tx
}

export const getExchangeRates = async (): Promise<IExchangeRates[]> => {
  const res = await fetch('/api/blockchain/ticker', {
    cache: 'force-cache',
  })

  if (!res.ok) {
    throw new Error('Failed to fetch rates data')
  }

  const tx: IExchangeRates[] = (await res.json()) as IExchangeRates[]

  return tx
}
