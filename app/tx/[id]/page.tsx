'use client'
import { useParams } from 'next/navigation'
import { ChangeEvent, useEffect, useState } from 'react'
import { getTransactionData } from '../../api'
import { BackButton } from '../../components'
import SpinningCircle from '../../components/SpinningCircle'
import { ITransaction, currencies, txInitialState } from '../../dto'
import {
  convertByCurrency,
  convertDateToLocale,
  getExchangeRate,
  reduceHashOrAddress,
} from '../../utils'

const Transaction = () => {
  const path = useParams()

  const [tx, setTx] = useState<ITransaction>(txInitialState)

  const [inputAmount, setInputAmount] = useState<number>(0)
  const [outputAmount, setOutputAmount] = useState<number>(0)
  const [isLoading, setIsLoading] = useState<boolean>(true)
  const [currency, setCurrency] = useState<string>('BTC')
  const [rate, setRate] = useState<number>(0)

  useEffect(() => {
    document.title = `Tx: ${reduceHashOrAddress(path.id)}`

    const fetchData = async () => {
      try {
        const data = await getTransactionData(path.id)
        setTx(() => ({ ...data }))
        const inputTotal: number = data.inputs.reduce(
          (acc, currentValue) => acc + currentValue.output_value,
          inputAmount,
        )
        setInputAmount(() => inputTotal)
        const outputTotal: number = data.outputs.reduce(
          (acc, currentValue) => acc + currentValue.value,
          outputAmount,
        )
        setOutputAmount(() => outputTotal)
        setIsLoading(() => !isLoading)
      } catch (error) {
        console.error(error)
      }
    }
    fetchData()
  }, [])

  useEffect(() => {
    const fetchData = async () => {
      if (currency !== '' && currency !== 'BTC') {
        const exchangeRate = await getExchangeRate(currency)
        setRate(() => exchangeRate)
      }
    }

    fetchData()
  }, [currency])

  const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
    setCurrency(() => event.target.value)
  }

  return (
    <>
      <main>
        <div className="max-w-md mx-auto rounded-xl bg-stone-100 shadow-md overflow-hidden md:max-w-2xl my-24">
          <div className="px-10 py-10">
            <BackButton />
            {isLoading ? (
              <div className="flex items-center justify-center">
                <SpinningCircle />
              </div>
            ) : (
              <>
                <div className="inline-flex justify-between w-full">
                  <div className="capitalize tracking-wide text-3xl text-black font-semibold">
                    Bitcoin Transaction
                  </div>
                  <select
                    className="block w-1/8 py-2 px-3 border border-gray-300 bg-stone-100 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
                    value={currency}
                    onChange={handleChange}>
                    {currencies.map((currency, index) => (
                      <option key={index} value={currency.type}>
                        {currency.type}
                      </option>
                    ))}
                  </select>
                </div>

                <div className="block mt-1 text-lg leading-tight font-medium text-black mt-3">
                  Hash ID
                </div>
                <p className="text-gray-500 truncate">{tx.hash}</p>
                <div className="inline-flex justify-between w-full">
                  <div>
                    <div className="mt-1 text-lg leading-tight font-medium text-black mt-3">
                      Received Time
                    </div>
                    <p className="text-gray-500">
                      {convertDateToLocale(tx.received, 'en-GB')}
                    </p>
                  </div>
                  <div>
                    <div className="mt-1 text-lg leading-tight font-medium text-black mt-3">
                      Size
                    </div>
                    <p className="text-gray-500">{tx.size} Bytes</p>
                  </div>
                </div>
                <div className="inline-flex justify-between w-full">
                  <div>
                    <div className="block mt-1 text-lg leading-tight font-medium text-black mt-3">
                      Gas
                    </div>
                    <p className="text-gray-500">
                      {convertByCurrency(tx.fees, currency, rate)}
                    </p>
                  </div>
                  <div>
                    <div className="block mt-1 text-lg leading-tight font-medium text-black mt-3">
                      Input
                    </div>
                    <p className="text-gray-500">
                      {convertByCurrency(inputAmount, currency, rate)}
                    </p>
                  </div>
                  <div>
                    <div className="block mt-1 text-lg leading-tight font-medium text-black mt-3">
                      Output
                    </div>
                    <p className="text-gray-500">
                      {convertByCurrency(outputAmount, currency, rate)}
                    </p>
                  </div>
                </div>
                <div className="inline-flex justify-between items-center w-full">
                  <div>
                    <span
                      className={`inline-flex items-center px-6 py-2 rounded-full text-sm font-medium ${
                        tx.confirmations > 0 ? 'bg-green-200' : 'bg-red-200'
                      } text-gray-800 mt-1`}>
                      {tx.confirmations > 0 ? 'Confirmed' : 'Pending'}
                    </span>
                  </div>
                  <div>
                    <div className="block mt-1 text-lg leading-tight font-medium text-black mt-3">
                      Confirmation
                    </div>
                    <p className="text-gray-500">{tx.confirmations}</p>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </main>
    </>
  )
}

export default Transaction
