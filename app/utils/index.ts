import { getExchangeRates } from '../api'
import {
  ICurrencies,
  IExchangeRate,
  IExchangeRates,
  IWebSockTxOut,
  currencies,
} from '../dto'

export const getBtcAmount = (amount: number): number => {
  const satoshisPerBTC = 100000000
  const btcAmount = amount ? amount / satoshisPerBTC : 0
  return btcAmount
}

export const reduceHashOrAddress = (value: string): string => {
  const start: string = value.slice(0, 4)
  const end: string = value.slice(value.length - 4, value.length)
  return `${start}-${end}`
}

export const convertDateToLocale = (date: string, format: string) => {
  const newDate = new Date(date)
  const converted = newDate.toLocaleDateString(format)
  return converted
}

export const convertToLocale = (date: number) => {
  const newDate = new Date(date * 1000)
  const converted = newDate.toLocaleString()
  return converted
}

// export const identifyWalletOrHash = (item: string): number => {
//   const walletAddressRegex =
//     /^(1|3|bc1)[0-9a-zA-Z]{25,}$|^(bc1)[0-9a-zA-Z]{39}$/

//   const transactionHashRegex = /^[A-Fa-f0-9]{64}$/

//   if (walletAddressRegex.test(item)) {
//     return 1
//   }
//   if (transactionHashRegex.test(item)) {
//     return 2
//   }
//   return 0
// }

export const getExchangeRate = async (currency: string): Promise<number> => {
  const rates: IExchangeRates[] = await getExchangeRates()
  const exchangeRate: IExchangeRate = rates[currency]
  if (exchangeRate === undefined) throw new Error('No conversion rate found')

  return exchangeRate.buy
}

export const convertByCurrency = (
  amount: number,
  symbol: string,
  rate: number,
): string => {
  if (symbol !== '' && symbol !== 'BTC') {
    const convertedAmount: number = getBtcAmount(amount) * rate
    const currency: ICurrencies = currencies.filter(
      (item) => item.type === symbol,
    )[0]
    if (currency === undefined)
      throw new Error('Something wrong with currency conversion')

    return `${currency.symbol}${convertedAmount.toFixed(2)}`
  }

  const fixedAmount =
    getBtcAmount(amount) >= 1
      ? getBtcAmount(amount).toFixed(2)
      : getBtcAmount(amount)

  return `${fixedAmount} ${symbol}`
}

export const calculateTotalFromOut = (list: IWebSockTxOut[]): number => {
  const total = list.reduce((acc, currentValue) => acc + currentValue.value, 0)
  return total
}
