export interface IGeneralPathProps {
  id: string
}

export interface Props {
  params: IGeneralPathProps
}

export interface ITxCardProps {
  hash: string
  time: number
  total: number
  rate: number
}
