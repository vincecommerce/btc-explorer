import { ITxCardProps } from '../props'
import {
  convertByCurrency,
  convertToLocale,
  reduceHashOrAddress,
} from '../utils'

export const TransactionListCard = (item: ITxCardProps) => {
  return (
    <div className="max-w-md mx-auto rounded-xl shadow-md overflow-hidden md:max-w-lg my-6">
      <div className="bg-gray-700 text-white px-6 py-4">
        <div className="flex justify-between mb-2">
          <span>{reduceHashOrAddress(item.hash)}</span>
          <span className="font-bold">
            {convertByCurrency(item.total, 'BTC', 0)}
          </span>
        </div>
        <div className="flex justify-between">
          <span>{convertToLocale(item.time)}</span>
          <span>{convertByCurrency(item.total, 'USD', item.rate)}</span>
        </div>
      </div>
    </div>
  )
}
