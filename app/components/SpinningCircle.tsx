const SpinningCircle = () => {
  return (
    <svg className="w-24 h-24 fill-current animate-spin" viewBox="0 0 24 24">
      <circle
        className="opacity-25"
        cx="12"
        cy="12"
        r="10"
        strokeWidth="2"
        stroke="currentColor"
        fill="none">
        <animate
          attributeName="stroke-dasharray"
          attributeType="XML"
          from="0 64"
          to="64 0"
          dur="750ms"
          repeatCount="indefinite"></animate>
      </circle>
    </svg>
  )
}

export default SpinningCircle
