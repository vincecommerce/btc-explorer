import { useRouter } from 'next/navigation'

export const BackButton = () => {
  const router = useRouter()

  function handleBackButtonClick() {
    router.back()
  }

  return (
    <div className="mb-2">
      <button
        onClick={handleBackButtonClick}
        className="inline-flex items-center px-2.5 py-1.5 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-stone-100 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
        <svg
          className="-ml-0.5 mr-2 h-4 w-4"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
          aria-hidden="true">
          <path
            fillRule="evenodd"
            d="M14.707 15.707a1 1 0 01-1.414 0l-6.364-6.364a1 1 0 010-1.414l6.364-6.364a1 1 0 111.414 1.414L8.12 9.293l6.293 6.293a1 1 0 010 1.414z"
            clipRule="evenodd"
          />
        </svg>
        <span>Back</span>
      </button>
    </div>
  )
}
