/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true,
  },
  async rewrites() {
    return [
      {
        source: '/api/cypher/:path*',
        destination: 'https://api.blockcypher.com/v1/btc/main/:path*',
      },
      {
        source: '/api/blockchain/:path*',
        destination: 'https://blockchain.info/:path*',
      },
    ]
  },
}

module.exports = nextConfig
