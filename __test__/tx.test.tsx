import '@testing-library/jest-dom/extend-expect'
import { fireEvent, render, screen } from '@testing-library/react'
import mockRouter from 'next-router-mock'
import { createDynamicRouteParser } from 'next-router-mock/dynamic-routes'
import { useRouter } from 'next/router'

jest.mock('next/router', () => require('next-router-mock'))

const ExampleComponent = ({ href = '' }) => {
  const router = useRouter()
  return (
    <button onClick={() => router.push(href)}>
      The current route is: "{router.asPath}"
    </button>
  )
}

mockRouter.useParser(createDynamicRouteParser(['/tx/[id]']))

describe('transaction page story', () => {
  test('from homepage to tx page', () => {
    mockRouter.push('/')

    render(
      <ExampleComponent href="/tx/a2ea45817399c1f31e77ebfe1c6c00a6660c02272dbd43ff7d4b300aa298e4e7" />,
    )

    expect(screen.getByRole('button')).toHaveTextContent(
      'The current route is: "/"',
    )

    fireEvent.click(screen.getByRole('button'))

    expect(mockRouter).toMatchObject({
      pathname: '/tx/[id]',
      query: {
        id: 'a2ea45817399c1f31e77ebfe1c6c00a6660c02272dbd43ff7d4b300aa298e4e7',
      },
    })
  })
})
