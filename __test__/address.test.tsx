import '@testing-library/jest-dom/extend-expect'
import { fireEvent, render, screen } from '@testing-library/react'
import mockRouter from 'next-router-mock'
import { createDynamicRouteParser } from 'next-router-mock/dynamic-routes'
import { useRouter } from 'next/router'

jest.mock('next/router', () => require('next-router-mock'))

const ExampleComponent = ({ href = '' }) => {
  const router = useRouter()
  return (
    <button onClick={() => router.push(href)}>
      The current route is: "{router.asPath}"
    </button>
  )
}

mockRouter.useParser(createDynamicRouteParser(['/address/[id]']))

describe('address page story', () => {
  test('from homepage to address page', () => {
    mockRouter.push('/')

    render(
      <ExampleComponent href="/address/bc1q6zyk5etsv8qy4rt8l9ljwx9y69ue5ynufmqz72" />,
    )

    expect(screen.getByRole('button')).toHaveTextContent(
      'The current route is: "/"',
    )

    fireEvent.click(screen.getByRole('button'))

    expect(mockRouter).toMatchObject({
      pathname: '/address/[id]',
      query: {
        id: 'bc1q6zyk5etsv8qy4rt8l9ljwx9y69ue5ynufmqz72',
      },
    })
  })
})
